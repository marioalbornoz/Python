from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.

def home(request):
    return render(request, "core/home.html")

def user(request):
    return render(request,"core/sesion.html")

def farmacos(request):
    return render(request,"core/farmacos.html")
