from django.contrib import admin
from .models import Droga, Funcionario

# Register your models here.
admin.site.register(Droga)
admin.site.register(Funcionario)