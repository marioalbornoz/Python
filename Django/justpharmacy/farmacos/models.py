from django.db import models

# Create your models here.
class Droga(models.Model):
    nombre = models.CharField(max_length=50)
    principio = models.CharField(max_length=50)
    descripcion = models.TextField(max_length=250)
    laboratorio = models.CharField(max_length=50)
    stock = models.IntegerField()

    def __str__(self):
        return self.nombre

class Funcionario(models.Model):
    nombre_funcionario = models.CharField(max_length=50)
    apellido_funcionario = models.CharField(max_length=50)
    droga = models.ManyToManyField(Droga, blank = True)
    
    def __str__(self):
        return self.nombre_funcionario

