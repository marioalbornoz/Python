from django.shortcuts import render
from .models import Droga
from django.views.generic import ListView

# Create your views here.

def index(request):
    return render(request, 'core/farmacos.html')

class FarmacoList(ListView):
    model = Droga
    template_name = 'core/farmaco_list.html'
    ordering = ['id']